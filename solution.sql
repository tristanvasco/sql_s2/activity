CREATE TABLE posts (
            id INT NOT NULL AUTO_INCREMENT,
            title VARCHAR(500) NOT NULL,
            content VARCHAR(500) NOT NULL,
            datetime_posted DATETIME NOT NULL,
            author_id INT NOT NULL,
            PRIMARY KEY (id),
            CONSTRAINT fk_posts_user_id
                FOREIGN KEY(author_id) REFERENCES users(id)
                ON UPDATE CASCADE
                ON DELETE RESTRICT
        );

CREATE TABLE users (
            id INT NOT NULL AUTO_INCREMENT,
            username VARCHAR(50) NOT NULL,
            password VARCHAR(50) NOT NULL,
            PRIMARY KEY (id)
        );

CREATE TABLE post_comments (
            id INT NOT NULL AUTO_INCREMENT,
            content VARCHAR(500) NOT NULL,
            datetime_posted DATETIME NOT NULL,
            post_id INT NOT NULL,
            user_id INT NOT NULL,
            PRIMARY KEY (id),
            CONSTRAINT fk_post_comments_post_id
                FOREIGN KEY(post_id) REFERENCES posts(id)
                ON UPDATE CASCADE
                ON DELETE RESTRICT,
            PRIMARY KEY (id),
            CONSTRAINT fk_post_comments_user_id
                FOREIGN KEY(user_id) REFERENCES users(id)
                ON UPDATE CASCADE
                ON DELETE RESTRICT
        );
CREATE TABLE posts_likes (
            id INT NOT NULL AUTO_INCREMENT,
            PRIMARY KEY (id),
            CONSTRAINT fk_posts_likes_post_id
                FOREIGN KEY(post_id) REFERENCES posts(id)
                ON UPDATE CASCADE
                ON DELETE RESTRICT,
            PRIMARY KEY (id),
            CONSTRAINT fk_posts_likes_user_id
                FOREIGN KEY(user_id) REFERENCES users(id)
                ON UPDATE CASCADE
                ON DELETE RESTRICT
        );

